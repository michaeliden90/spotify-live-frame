const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin("[name]");

module.exports = {
    entry: {
        "build/styles.css": "./scss/styles.scss",
        "build/main.js": "./js/main.js"
    },
    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    }, {
                        loader: "sass-loader",
                        options: {
                            includePaths: [
                                "scss",
                                "node_modules/"
                            ],
                            sourceMap: true,
                            outputStyle: "expanded"
                        }
                    }],
                    fallback: "style-loader"
                })
            },
            {
                test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
                loader: "url-loader?name=./dist/fonts/[hash].[ext]"
            }
        ]
    }, devServer: {
        contentBase: './build'
    },
    plugins: [
        extractSass
    ],
    output: {
        filename: "[name]"
    },
    stats: {
        colors: true
    },
    node: {
        console: true,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
    devtool: 'inline-source-map',
};