import $ from "jquery"
import SpotifyWebApi from "spotify-web-api-js"
import queryString from "query-string"

// My Echo
const requestOptions = {"device_id": "dfc06bc46b01677b84f5c9524a9870da678b2b2a"};
const ACCOUNT_URI = "https://accounts.spotify.com/authorize?";
const params = {
    "client_id": "88b2b94b22674ce781e9e2cf12903d96",
    "response_type": "token",
    "redirect_uri": "http://localhost:8080/",
    "scope": "user-read-playback-state user-modify-playback-state user-read-currently-playing user-library-read"
};
const refreshTime = 1800000; //30 Seconds

let accessToken = queryString.parse(location.hash)['access_token'];
let isPlaying = false;
let spotifyApi = new SpotifyWebApi();
if(accessToken) {
    spotifyApi.setAccessToken(accessToken);
    setPlaybackState();
    setInterval(refreshAccess, refreshTime);
} else {
    refreshAccess();
}

function refreshAccess() {
    window.location.href = ACCOUNT_URI + queryString.stringify(params);
}

function setPlayState() {
    isPlaying = true;
    $("#play-pause svg").removeClass("fa-play").addClass("fa-pause");
}

function setPauseState() {
    isPlaying = false;
    $("#play-pause svg").removeClass("fa-pause").addClass("fa-play");
}

function setPlaybackState() {
    spotifyApi.getMyCurrentPlaybackState({}, (err, data) => {
        if (err) {
            refreshAccess();
        } else {
            setTrackState(data);
            if (data["is_playing"]) {
                setPlayState();
            } else {
                setPauseState();
            }
        }
    });
}

function setTrackState(data) {
    $("#cover").attr("src", data["item"]["album"]["images"][0]["url"]);
    $("#title").text(data["item"]["name"]);
    $("#artist").text(data["item"]["artists"].reduce(function(res, v) {
        return res.concat(v["name"]);
    }, []).join(", "))
}

setInterval(setPlaybackState, 2000);

$("#play-pause").on('click', () => {
    if (isPlaying) {
        spotifyApi.pause(requestOptions, setPauseState);
    } else {
        spotifyApi.play(requestOptions, setPlayState);
    }
});

$("#prev").on('click', () => {
    spotifyApi.skipToPrevious(requestOptions, (err,data) => {
        setTimeout(setPlaybackState, 1000);
    })
});

$("#next").on('click', () => {
    spotifyApi.skipToNext(requestOptions, (err,data) => {
        setTimeout(setPlaybackState, 1000);
    })
});
